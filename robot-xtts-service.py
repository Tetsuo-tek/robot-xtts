#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.abstractflow                  import FlowChannel
from PySketch.flowsync                      import FlowSync
from PySketch.flowproto                     import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat                       import FlowSat
from PySketch.elapsedtime                   import ElapsedTime

###############################################################################

import sys
import argparse
import json
import re
import time
import langid
import librosa
import torch
import torchaudio

import torch.nn.functional                  as F
import numpy                                as np

from queue                                  import Queue
from coqpit                                 import Coqpit
from dataclasses                            import dataclass
from TTS.tts.configs.xtts_config            import XttsConfig
from TTS.tts.models.xtts                    import Xtts
from TTS.tts.layers.xtts.gpt                import GPT
from TTS.tts.layers.xtts.hifigan_decoder    import HifiDecoder
from TTS.tts.layers.xtts.stream_generator   import init_stream_support
from TTS.tts.layers.xtts.tokenizer          import VoiceBpeTokenizer, split_sentence
from TTS.tts.layers.xtts.xtts_manager       import SpeakerManager, LanguageManager
from TTS.tts.models.base_tts                import BaseTTS
from TTS.utils.io                           import load_fsspec

###############################################################################
# SKETCH

sat = FlowSat()
t = 0.005

active = False

promptChanName = "TTS.Prompt"
promptChan = None
promptPack = None
currentPrompt = None
lastCmdHash = None

vocabByPhoneme = None
wavChuncks = Queue()

inputChanNane = None
inputChan = None
inputQueue = Queue()

outputChanName = "TTS.PCM"
outputChan = None

useDeepSpeed = False

closing = False

def setup():
    global args
    global inputChanNane
    global useDeepSpeed

    print("[SETUP] ..")

    parser = argparse.ArgumentParser(description="Robot XTTS service")
    parser.add_argument('sketchfile', help='Sketch program file')
    parser.add_argument('--user', help='Flow-network username', default='guest')
    parser.add_argument('--password', help='Flow-network password', default='password')
    parser.add_argument('--text-input-src', help='Text input flow-channel', default='guest.Sentences.TXT')
    parser.add_argument('--deepspeed', action="store_true", help='Applying optimizations by deepspeed library (require to install deepspeed)')

    args = parser.parse_args()
    
    inputChanNane = args.text_input_src
    useDeepSpeed = args.deepspeed

    sat.setLogin(args.user, args.password)

    sat.setNewChanCallBack(onChannelAdded)
    sat.setDelChanCallBack(onChannelRemoved)
    sat.setStartChanPubReqCallBack(onStartChanPub)
    sat.setStopChanPubReqCallBack(onStopChanPub)
    sat.setGrabDataCallBack(onDataGrabbed)
    sat.setRequestCallBack(onPromptServiceRequest)

    ok = sat.connect() # uses the env-var ROBOT_ADDRESS

    if ok:
        sat.setSpeedMonitorEnabled(False)
        print("[LOOP] ..")

        sat.addServiceChannel(promptChanName)
        sat.addStreamingChannel(Flow_T.FT_AUDIO_DATA, Variant_T.T_BYTEARRAY, outputChanName)
    
    return ok
    
def loop():
    global lastCmdHash
    global wavChuncks

    #if wavChuncks.qsize() > 2 or (currentPrompt is None and not wavChuncks.empty()):
    if not wavChuncks.empty():
        chunk = wavChuncks.get_nowait()
        print("* publishing audio chunk [{}]".format(len(chunk)))
        pcm = np.int16(chunk.cpu().numpy() * 32767).tobytes()
        sat.publish(outputChan.chanID, pcm)
        
    sat.tick()
    return sat.isConnected()

###############################################################################
# CALLBACKs

def onChannelAdded(ch):
    global thread
    global promptChan
    global inputChan
    global outputChan

    print("Channel ADDED: {}".format(ch.name))

    if ch.name == "{}.{}".format(sat._userName, promptChanName):
        promptChan = ch
        print("PromptService is READY: {}".format(promptChan.name))
    
    elif ch.name == "{}.{}".format(sat._userName, outputChanName):
        outputChan = ch
        print("OutputChan is READY: {}".format(outputChan.name))

        sat.setTickTimer(t, t * 40)

    elif ch.name == inputChanNane:
        inputChan = ch
        print("InputChan is READY: {}".format(inputChan.name))
        sat.subscribeChannel(inputChan.chanID)

def onChannelRemoved(ch):
    print("Channel REMOVED: {}".format(ch.name))

def onStartChanPub(ch):
    print("Publish START required: {}".format(ch.name))

def onStopChanPub(ch):
    print("Publish STOP required: {}".format(ch.name))

def onDataGrabbed(chanID, data):
    if chanID == inputChan.chanID:
        inputQueue.put(data.decode('utf-8'))
    
###############################################################################

def onPromptServiceRequest(chanID, hash, cmdName, val):
    global promptPack
    global currentPrompt
    global lastCmdHash

    if promptChan.chanID == chanID:
        if cmdName == "SPEECH":
            if currentPrompt is None:
                lastCmdHash = hash
                promptPack = val["value"]
                print("PromptPack RECEIVED [cmdName: {}; hash: {}]: {}".format(cmdName, lastCmdHash, promptPack))

            else:
                sat.sendServiceResponse(chanID, hash, {"status" : False})
                print("CANNOT accept the prompt -> another evaluation is going on background")
        
###############################################################################
# Inference

def inferenceRunner():
    global currentPrompt
    global promptPack
    global wavChuncks
    global vocabByPhoneme
    
    print("PyTorch Version:", torch.__version__)

    if torch.cuda.is_available():
        print("CUDA is available")
        torch.cuda.init()
        #device = torch.cuda.device(0)

    else:
        print("CUDA is NOT available")

    print("Loading model config ..")

    config = XttsConfig()
    config.load_json("config.json")

    print("Loading model ..")

    #chkPoint = "/home/roko/.local/share/tts/tts_models--multilingual--multi-dataset--xtts_v2/"
    chkPoint = "/home/ddiottavio/.local/share/tts/tts_models--multilingual--multi-dataset--xtts_v2/"

    ##
    vocabByPhoneme = None

    with open("{}vocab.json".format(chkPoint), 'r') as file:
        vocabByPhoneme = json.load(file)

    vocabByPhoneme = vocabByPhoneme["model"]["vocab"]
    vocabByID = {}

    for key, value in vocabByPhoneme.items():
        vocabByID[int(value)] = key
    ##

    model = Xtts.init_from_config(config)

    if useDeepSpeed:
        model.load_checkpoint(config, checkpoint_dir=chkPoint, use_deepspeed=True)
    else:
        model.load_checkpoint(config, checkpoint_dir=chkPoint, eval=True)

    if torch.cuda.is_available():
        #model.cuda()
        model.to("cuda")

    print("Compute device: {}".format(model.device))
    print("Computing speaker latents ..")

    gpt_cond_latent, speaker_embedding = model.get_conditioning_latents(
        #audio_path=["voices/output-Gitta Nikolina.wav", "voices/output-Rosemary Okafor.wav", "voices/output-Maja Ruoho.wav"],
        #audio_path=["voices/output-Gitta Nikolina.wav"],
        #audio_path=["voices/output-Baldur Sanjin.wav"],
        #audio_path=["voices/output-Tammie Ema.wav"],
        #audio_path=["voices/output-Alexandra Hisakawa.wav"],
        #audio_path=["voices/output-Alison Dietlinde.wav"],
        #audio_path=["voices/output-Ana Florence.wav"],
        #audio_path=["voices/output-Camilla Holmström.wav"],
        audio_path=["voices/output-Dionisio Schuyler.wav"],
        max_ref_length=10,
        gpt_cond_len=6,
        gpt_cond_chunk_len=6,
        librosa_trim_db=None,
        sound_norm_refs=False,
        load_sr=24000
    )

    print("READY")
    active = True

    while True:
        if inputQueue.empty():
            if promptPack is None:
                time.sleep(t)
                continue

            else:
                currentPrompt = promptPack["prompt"]
                promptPack = None

        else:
            currentPrompt = inputQueue.get()

        currentPrompt = currentPrompt.strip()
        substitutes = [r'\*\*', r'\*', r'\#', r'\#\#', r'\"']

        for seq in substitutes:
            currentPrompt = re.sub(seq, '', currentPrompt)
        
        currentPrompt = currentPrompt.replace('.', ';')

        '''
        if currentPrompt.endswith('.'):
            currentPrompt = currentPrompt[:-1]
            currentPrompt += ';'
            currentPrompt = currentPrompt.replace('"', '')
        '''
        
        '''
        language_predicted = langid.classify(currentPrompt)[0].strip()  # strip need as there is space at end!

        # tts expects chinese as zh-cn
        if language_predicted == "zh":
            language_predicted = "zh-cn"
        '''
        language_predicted = "it"
        print("Detected language: {}".format(language_predicted))
        print("Prompt: {}".format(currentPrompt))
        
        chrono = ElapsedTime()
        chrono.start()

        #chunks = model.inference_stream(
        chunks = inference_stream(model,
            text=currentPrompt,
            language=language_predicted,
            gpt_cond_latent=gpt_cond_latent,
            speaker_embedding=speaker_embedding,
            stream_chunk_size=60,
            overlap_wav_len=1024,
            enable_text_splitting=True,
            temperature=1.7,
            top_k=70,
            top_p=0.25,
            length_penalty=1.0,
            repetition_penalty=10.0,
            do_sample=True,
            speed=1.2
        )
        
        bufferChuncks = 1
        buffer = Queue()

        for i, chunk in enumerate(chunks):
            if i < bufferChuncks:
                print("Buffering {} [samples: {}]".format(i, chunk.shape[-1]))
                buffer.put(chunk)
                continue

            elif i == bufferChuncks:
                print("Flushing buffer ..") 

                while not buffer.empty():
                     wavChuncks.put(buffer.get())

            print("Chunk {} [samples: {}]".format(i, chunk.shape[-1]))
            wavChuncks.put(chunk)

        if not buffer.empty():
            while not buffer.empty():
                wavChuncks.put(buffer.get())

        print("inference terminated: {} s".format(chrono.stop()))
        print("Tokens [{}]:".format(len(allTokens)))

        for e in allTokens:
            print("\t{} - {} {}".format(i, e, vocabByID[e]))
            i += 1
            
        currentPrompt = None

###############################################################################

def main():
    inferenceRunner()

###############################################################################
# Xtts EXTERNALIZED METHODs

allTokens = []

@torch.inference_mode()
def inference_stream(
        model,
        text,
        language,
        gpt_cond_latent,
        speaker_embedding,
        # Streaming
        stream_chunk_size=20,
        overlap_wav_len=1024,
        # GPT inference
        temperature=0.75,
        length_penalty=1.0,
        repetition_penalty=10.0,
        top_k=50,
        top_p=0.85,
        do_sample=True,
        speed=1.0,
        enable_text_splitting=False,
        **hf_generate_kwargs,
    ):
        global allTokens

        allTokens = []

        language = language.split("-")[0]  # remove the country code
        length_scale = 1.0 / max(speed, 0.05)

        gpt_cond_latent = gpt_cond_latent.to(model.device)
        speaker_embedding = speaker_embedding.to(model.device)

        if enable_text_splitting:
            text = split_sentence(text, language, model.tokenizer.char_limits[language])
            
        else:
            text = [text]
        
        i = 0

        for sent in text:
            #print("!!!!!!!", i, sent)
            sent = sent.strip().lower()

            tokens = model.tokenizer.encode(sent, lang=language)
            allTokens.extend(tokens)
            print("* sent: {}".format(sent))
            print("* tokens: {}".format(tokens))

            text_tokens = torch.IntTensor(tokens).unsqueeze(0).to(model.device)

            #x = text_tokens.cpu().numpy()
            #np.savetxt(f"tt_{i}.csv", x, delimiter=",")

            assert (
                text_tokens.shape[-1] < model.args.gpt_max_text_tokens
            ), " ❗ XTTS can only generate text with a maximum of 400 tokens."

            fake_inputs = model.gpt.compute_embeddings(
                gpt_cond_latent.to(model.device),
                text_tokens,
            )

            gpt_generator = model.gpt.get_generator(
                fake_inputs=fake_inputs,
                top_k=top_k,
                top_p=top_p,
                temperature=temperature,
                do_sample=do_sample,
                num_beams=1,
                num_return_sequences=1,
                length_penalty=float(length_penalty),
                repetition_penalty=float(repetition_penalty),
                output_attentions=False,
                output_hidden_states=True,
                **hf_generate_kwargs,
            )

            last_tokens = []
            all_latents = []
            wav_gen_prev = None
            wav_overlap = None
            is_end = False

            z = 0
            while not is_end:
                try:
                    x, latent = next(gpt_generator)

                    last_tokens += [x]
                    all_latents += [latent]
                    
                    #o = x.cpu().numpy()
                    #print(x.item())

                    '''
                    np.savetxt(f"tt_{z}_gpt_latents.csv", o, delimiter=",")
                    z += 1
                    '''

                except StopIteration:
                    is_end = True

                if is_end or (stream_chunk_size > 0 and len(last_tokens) >= stream_chunk_size):
                    gpt_latents = torch.cat(all_latents, dim=0)[None, :]

                    if length_scale != 1.0:
                        gpt_latents = F.interpolate(
                            gpt_latents.transpose(1, 2),
                            scale_factor=length_scale,
                            mode="linear"
                        ).transpose(1, 2)

                    #o = gpt_latents.cpu().numpy()[0][0]
                    #print(len(o))
                    #np.savetxt(f"tt_{z}_gpt_latents.csv", o, delimiter=",")
                    #z += 1

                    wav_gen = model.hifigan_decoder(gpt_latents, g=speaker_embedding.to(model.device))

                    wav_chunk, wav_gen_prev, wav_overlap = model.handle_chunks(
                        wav_gen.squeeze(),
                        wav_gen_prev,
                        wav_overlap,
                        overlap_wav_len
                    )

                    print(len(last_tokens))
                    last_tokens = []
                    yield wav_chunk

            i += 1
            
###################################################################
