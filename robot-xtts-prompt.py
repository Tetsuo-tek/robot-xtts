#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.abstractflow import FlowChannel
from PySketch.flowsync import FlowSync
from PySketch.flowproto import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat import FlowSat

###############################################################################

import sys
import select
import argparse
import time
import pyaudio

import numpy as np

from queue import Queue

###############################################################################
# SKETCH

sat = FlowSat()

promptChanName = None
promptServiceUser = None
promptChan = None

pcmChanName = None
pcmChan = None

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 22050
CHUNK = 1024
CHUNK_SZ = CHUNK*2

audio = pyaudio.PyAudio()
playback = None

wavChuncks = Queue()

#active = True

def setup():
    global promptChanName
    global promptServiceUser
    global pcmChanName
    global playback
    global t

    print("[SETUP] ..")

    parser = argparse.ArgumentParser(description="Robot XTTS prompt")
    parser.add_argument('sketchfile', help='Sketch program file')
    parser.add_argument('--user', help='Flow-network username', default='guest')
    parser.add_argument('--password', help='Flow-network password', default='password')
    parser.add_argument('--service-name', help='The Promp service channel', default='guest.TTS.Prompt')

    args = parser.parse_args()
    
    sat.setLogin(args.user, args.password)

    t = 0.005 # seconds
    sat.setTickTimer(t, t * 50)

    sat.setResponseCallBack(onPromptServiceResponse)

    sat.setNewChanCallBack(onChannelAdded)
    sat.setDelChanCallBack(onChannelRemoved)
    
    sat.setGrabDataCallBack(onDataGrabbed)

    promptChanName = args.service_name
    promptServiceUser = promptChanName.split(".")[0]
    pcmChanName = "{}.TTS.PCM".format(promptServiceUser)

    ok = sat.connect()

    if ok:
        sat.setSpeedMonitorEnabled(True)

        playback = audio.open(
            format=FORMAT,
            channels=CHANNELS, rate=RATE,
            frames_per_buffer=CHUNK,
            input=False,
            output=True,
            stream_callback=playBuffer
        )

        playback.start_stream()

        print("[LOOP] ..")
    
    return ok

def loop():
    i, _, _ = select.select([sys.stdin], [], [], 0.0)

    if i:
        prompt = sys.stdin.readline().strip()
        promptPack = {"prompt" : "{}".format(prompt)}

        if len(prompt) == 0:
            print("Exiting ..")
            return False

        if prompt == "!":
            sat.sendServiceRequest(promptChan.chanID, "STOP_GEN", {})

        else:
            sat.sendServiceRequest(promptChan.chanID, "SPEECH", promptPack)

    sat.tick()

    return sat.isConnected()

def close():
    if playback is not None:
        playback.stop_stream()
        playback.close()
        audio.terminate()

###############################################################################
# CALLBACKs

def onPromptServiceResponse(chanID, response):
    if not response["status"]:
        print("Prompt ERROR: {}".format(response))

    sys.stdout.write("\n> ")
    sys.stdout.flush()

def onChannelAdded(ch):
    global promptChan
    global pcmChan

    if ch.name == promptChanName:
        promptChan = ch
        print("PromptService FOUND: {}".format(promptChan.name))

    elif ch.name == pcmChanName:
        pcmChan = ch
        sat.subscribeChannel(pcmChan.chanID)

def onChannelRemoved(ch):
    global promptChan

    if ch.name == promptChanName:
        print("PromptService CLOSED: {}".format(promptChan.name))
        promptChan = None

def onDataGrabbed(chanID, data):
    global pcmChan

    if pcmChan is None:
        return
    
    if pcmChan.chanID == chanID:
        l = len(data)
        print("Data come [{} B - buffers: {}]".format(l, (l/CHUNK_SZ)))

        for i in range(0, l, CHUNK_SZ):
            to = i+CHUNK_SZ
            chunk = data[i:to]
            wavChuncks.put(np.frombuffer(chunk, dtype=np.int16))

###############################################################################
# PLAYBACK

def playBuffer(input_data, frame_count, time_info, status_flag):
    if wavChuncks.empty():
        return (np.zeros(frame_count, dtype=np.int16), pyaudio.paContinue)

    #print("Chunk [{} requested]".format(frame_count))
    data = wavChuncks.get_nowait()
    return (data, pyaudio.paContinue)

###############################################################################
