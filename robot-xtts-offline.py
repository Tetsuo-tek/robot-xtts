#!/usr/bin/env python

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

#from TTS.api import TTS

import os
import torch
import torchaudio

from TTS.tts.configs.xtts_config import XttsConfig
from TTS.tts.models.xtts import Xtts

from PySketch.elapsedtime import ElapsedTime
from PySketch.log import LogMachine, msg, dbg, wrn, err, cri, printPair

import argparse

LogMachine()

def parseArguments():
    parser = argparse.ArgumentParser(description="TTS offline")
    parser.add_argument("--speaker", help="Select speaker wav file", default="voices/output-Dionisio Schuyler.wav")
    parser.add_argument("--language", help="Select speaker language", default="it")
    parser.add_argument("--speed", help="Select speech speed", default=1)
    parser.add_argument("--text-file", help="Select input text file (Text file with N text-blocks separated by '\\n\\n', produces N wav files)")
    parser.add_argument("--output", help="Base name for output files", default="output.wav")
    parser.add_argument("--cuda", action="store_true", help="Use NVIDIA gpu")
    
    return parser.parse_args()

def readTextFile(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        return file.read()

def generateFilePath(index, totalBlocks, outputBase):
    numDigits = len(str(totalBlocks))
    formattedIndex = f"{index:0{numDigits}}"
    return f"{formattedIndex}-{outputBase}"
'''
def generate(tts, blocks, args):
    totalBlocks = len(blocks)

    msg("Generating {} wav files ..".format(totalBlocks))

    for i, blk in enumerate(blocks):
        blk = blk.strip()

        if len(blk) == 0:
            continue

        filePath = generateFilePath(i, totalBlocks, args.output)

        msg("Generating {} ..".format(filePath))
        tts.tts_to_file(text=blk, speaker_wav=args.speaker, language=args.language, speed=args.speed, file_path=filePath)
'''
def generate(model, blocks, args):
    chrono = ElapsedTime()
    totalBlocks = len(blocks)

    msg("Computing speaker latents...")
    gpt_cond_latent, speaker_embedding = model.get_conditioning_latents(
        audio_path=[args.speaker],
        max_ref_length=10,
        gpt_cond_len=6,
        gpt_cond_chunk_len=6,
        librosa_trim_db=None,
        sound_norm_refs=False,
        load_sr=24000
    )

    msg("Creating {} wav files ..".format(totalBlocks))

    for i, blk in enumerate(blocks):
        blk = blk.strip()
        filePath = generateFilePath(i, totalBlocks, args.output)
        msg("Text: '{}'".format(blk))

        chrono.start()

        out = model.inference(
            text=blk,
            language=args.language,
            gpt_cond_latent=gpt_cond_latent,
            speaker_embedding=speaker_embedding,
            temperature=0.7,
        )

        torchaudio.save(filePath, torch.tensor(out["wav"]).unsqueeze(0), 24000)
        dbg("Last generation time [{}]: {} s".format(filePath, int(chrono.stop())))

def main():
    args = parseArguments()
    msg("Initializing XTTS_v2 model ..")
    '''

    if args.cuda:
        tts = TTS("tts_models/multilingual/multi-dataset/xtts_v2").to("cuda")

    else:
        tts = TTS("tts_models/multilingual/multi-dataset/xtts_v2").to("cpu")

    data = readTextFile(args.text_file)
    blocks = data.split("\n\n")
    generate(tts, blocks, args)
    '''

    chkPoint = "/home/ddiottavio/.local/share/tts/tts_models--multilingual--multi-dataset--xtts_v2/"

    config = XttsConfig()
    config.load_json("{}config.json".format(chkPoint))
    model = Xtts.init_from_config(config)
    model.load_checkpoint(config, checkpoint_dir=chkPoint)#, use_deepspeed=True)

    if args.cuda:
        model.cuda()

    data = readTextFile(args.text_file)
    blocks = data.split("\n\n")

    chrono = ElapsedTime()
    chrono.start()
    generate(model, blocks, args)

    msg("Finished [{} s]".format(int(chrono.stop())))

if __name__ == "__main__":
    main()
